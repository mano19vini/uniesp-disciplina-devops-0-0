terraform {
  backend "gcs" {
    bucket      = "tfstate-mba-uniesp"
    prefix      = "dev/state"
    credentials = "user.json"
  }
}