variable "project" {
  description = "Nome do projeto na cloud"
}
variable "region" {
  description = "Definindo a região da cloud"
}

variable "zone" {
  description = "Definindo a zona da cloud"
}

variable "cluster_name" {
  description = "Nome do cluster"
  default     = "k8s-gke-uniesp"
}

variable "nodes_count" {
  description = "Quantidade de nos"
}

variable "credentials" {
  description = "Credential de acesso"
}

variable "external_tags" {
  description = "Tags dos recursos"
}



